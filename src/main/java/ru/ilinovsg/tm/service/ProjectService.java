package ru.ilinovsg.tm.service;

import ru.ilinovsg.tm.exception.ProjectNotFoundException;
import ru.ilinovsg.tm.repository.ProjectRepository;
import ru.ilinovsg.tm.entity.Project;

import java.util.List;

public class ProjectService extends AbstractService{

    private ProjectService() {
    }

    private static ProjectService instance = null;

    public static ProjectService getInstance(){
        synchronized (ProjectService.class) {
            if (instance == null) {
                instance = new ProjectService();
            }
        }
        return instance;
    }

    ProjectRepository projectRepository = ProjectRepository.getInstance();
    UserProjectTaskService userProjectTaskService = UserProjectTaskService.getInstance();

    public Project create(String name, String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.create(name, description);
    }

    public int createProject() {
        System.out.println("[Create project]");
        System.out.println("[Please, enter project name]");
        final String name = scanner.nextLine();
        System.out.println("[Please, enter project description]");
        final String description = scanner.nextLine();
        create(name, description);
        System.out.println("[OK]");
        return 0;
    }

    public Project update(Long id, String name, String description) throws ProjectNotFoundException {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.update(id, name, description);
    }

    public int updateProjectByName() throws ProjectNotFoundException {
        System.out.println("[Update project]");
        System.out.println("[Please, enter project name]");
        final String name = scanner.nextLine();
        final Project project = findByName(name);
        if(project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[Please, enter project name]");
        final String new_name = scanner.nextLine();
        System.out.println("[Please, enter project description]");
        final String description = scanner.nextLine();
        update(project.getId(), new_name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int updateProjectById() throws ProjectNotFoundException {
        System.out.println("[Update project]");
        System.out.println("[Please, enter project id]");
        final Long id = Long.parseLong(scanner.nextLine());
        final Project project = findById(id);
        if(project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[Please, enter project name]");
        final String name = scanner.nextLine();
        System.out.println("[Please, enter project description]");
        final String description = scanner.nextLine();
        update(project.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int updateProjectByIndex() throws ProjectNotFoundException {
        System.out.println("[Update project]");
        System.out.println("[Please, enter project index]");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Project project = findByIndex(index);
        if(project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[Please, enter project name]");
        final String name = scanner.nextLine();
        System.out.println("[Please, enter project description]");
        final String description = scanner.nextLine();
        update(project.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public Project findByIndex(int index) {
        if (index < 0 || index > projectRepository.getSize() - 1) return null;
        return projectRepository.findByIndex(index);
    }

    public Project removeByIndex(int index) {
        if (index < 0 || index > projectRepository.getSize() - 1) return null;
        return projectRepository.removeByIndex(index);
    }

    public int removeProjectByIndex() {
        System.out.println("[Please, enter project index]");
        final int index = scanner.nextInt() - 1;
        final Project project = removeByIndex(index);
        if(project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int viewProjectByIndex() {
        System.out.println("Enter project index");
        final int index = scanner.nextInt() - 1;
        final Project project = findByIndex(index);
        viewProject(project);
        return 0;
    }

    public Project findByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name);
    }

    public Project removeByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(name);
    }

    public int viewProjectByName() {
        System.out.println("Enter project name");
        final String name = scanner.nextLine();
        final Project project = findByName(name);
        viewProject(project);
        return 0;
    }

    public int removeProjectByName() {
        System.out.println("[Please, enter project name]");
        final String name = scanner.nextLine();
        final Project project = removeByName(name);
        if(project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public Project findById(Long id) throws ProjectNotFoundException {
        if (id == null) return null;
        final Project project = projectRepository.findById(id);
        if (project == null) throw new ProjectNotFoundException("Project not found");
        return project;
    }

    public Project removeById(Long id) throws ProjectNotFoundException {
        if (id == null) return null;
        return projectRepository.removeById(id);
    }

    public int viewProjectById() throws ProjectNotFoundException {
        System.out.println("Enter project id");
        final Long id = scanner.nextLong();
        final Project project = findById(id);
        viewProject(project);
        return 0;
    }

    public int removeProjectById() throws ProjectNotFoundException {
        System.out.println("[Please, enter project id]");
        final Long id = scanner.nextLong();
        final Project project;
        project = removeById(id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int clearProject() {
        System.out.println("[Clear project]");
        projectRepository.clear();
        System.out.println("[OK]");
        return 0;
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public int listProject() {
        System.out.println("[List project]");
        int index = 1;
        for(final Project project: findAll()) {
            System.out.println(index + "." + project.getId() + " " + project.getName());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    public void viewProject(final Project project) {
        if(project == null) return;
        System.out.println("[View project]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
    }

    public void viewProjects(final List<Project> projects) {
        if (projects == null || projects.isEmpty()) return;
        int index = 1;
        for(final Project project: projects) {
            if (project.getUserId() == null)
                System.out.println(index + "." + project.getId() + " " + project.getName());
            else
                System.out.println(index + "." + project.getId() + " " + project.getName() + " " + project.getUserId());
            index++;
        }
    }

    public List<Project> findAllByUserId(Long userId) {
        if (userId == null) return null;
        return projectRepository.findAllByUserId(userId);
    }

    public int listProjectByUserId() {
        System.out.println("[List project by user id]");
        System.out.println("[Please, enter user id]");
        final Long userId = Long.parseLong(scanner.nextLine());
        final List<Project> projects = findAllByUserId(userId);
        viewProjects(projects);
        System.out.println("[OK]");
        return 0;
    }

    public int addProjectToUserByIds() throws ProjectNotFoundException {
        System.out.println("[Add project to user by ids]");
        System.out.println("[Please, enter user id]");
        final Long userId = Long.parseLong(scanner.nextLine());
        System.out.println("[Please, enter project id]");
        final Long projectId = Long.parseLong(scanner.nextLine());
        userProjectTaskService.addProjectToUser(userId, projectId);
        System.out.println("[OK]");
        return 0;
    }

    public int removeProjectFromUserByIds() {
        System.out.println("[Remove project from user by id]");
        System.out.println("[Please, enter user id]");
        final Long userId = Long.parseLong(scanner.nextLine());
        System.out.println("[Please, enter project id]");
        final Long projectId = Long.parseLong(scanner.nextLine());
        userProjectTaskService.removeProjectFromUser(userId, projectId);
        System.out.println("[OK]");
        return 0;
    }

}
