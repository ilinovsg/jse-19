package ru.ilinovsg.tm;

import ru.ilinovsg.tm.enumerated.Role;
import ru.ilinovsg.tm.observer.*;
import ru.ilinovsg.tm.repository.ProjectRepository;
import ru.ilinovsg.tm.repository.TaskRepository;
import ru.ilinovsg.tm.repository.UserRepository;
import ru.ilinovsg.tm.service.*;
import ru.ilinovsg.tm.utils.hashMD5;

public class App {
    ProjectRepository projectRepository = ProjectRepository.getInstance();
    ProjectService projectService = ProjectService.getInstance();
    TaskRepository taskRepository = TaskRepository.getInstance();
    TaskService taskService = TaskService.getInstance();
    UserRepository userRepository = UserRepository.getInstance();
    ProjectTaskService projectTaskService = ProjectTaskService.getInstance();

    {
        projectRepository.create("PROJECT 3");
        projectRepository.create("PROJECT 1");
        projectRepository.create("PROJECT 2");
        taskRepository.create("TASK 3");
        taskRepository.create("TASK 1");
        taskRepository.create("TASK 2");

        userRepository.create("ADMIN", hashMD5.md5("123QWE"), "Ivan", "Ivanov", Role.Admin);
        userRepository.create("TEST", hashMD5.md5("123"), "Peter", "Petrov", Role.User);
    }

    public static void main(final String[] args) {
        new App();
        displayWelcome();
        Publisher publisher = new PublisherImpl();
        ListenerProjectImpl listenerProject = new  ListenerProjectImpl();
        ListernerTaskImpl listenerTask = new ListernerTaskImpl();
        ListenerSystemImpl listenerSystem = new ListenerSystemImpl();
        ListenerUserImpl listenerUser = new ListenerUserImpl();
        publisher.addListener(listenerProject);
        publisher.addListener(listenerTask);
        publisher.addListener(listenerSystem);
        publisher.addListener(listenerUser);
        publisher.start();
    }

    public static void displayWelcome() {
        System.out.println("***WELCOME TO TASK-MANAGER***");
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public ProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }
}
