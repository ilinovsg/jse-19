package ru.ilinovsg.tm.observer;

import ru.ilinovsg.tm.service.SystemService;

import static ru.ilinovsg.tm.constant.TerminalConst.*;
import static ru.ilinovsg.tm.constant.TerminalConst.EXIT;

public class ListenerSystemImpl implements Listener{
    @Override
    public int update(String command) {
        SystemService systemService = SystemService.getInstance();

        switch (command) {
            case VERSION:
                return systemService.displayVersion();
            case ABOUT:
                return systemService.displayAbout();
            case HELP:
                return systemService.displayHelp();
            case EXIT:
                return systemService.displayExit();
        }
        return -1;
    }
}
