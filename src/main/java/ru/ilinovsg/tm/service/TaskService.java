package ru.ilinovsg.tm.service;

import ru.ilinovsg.tm.exception.ProjectNotFoundException;
import ru.ilinovsg.tm.exception.TaskNotFoundException;
import ru.ilinovsg.tm.repository.TaskRepository;
import ru.ilinovsg.tm.entity.Task;

import java.util.List;

public class TaskService extends AbstractService{

    private TaskService() {
    }

    private static TaskService instance = null;

    public static TaskService getInstance(){
        synchronized (TaskService.class) {
            if (instance == null) {
                instance = new TaskService();
            }
        }
        return instance;
    }

    TaskRepository taskRepository = TaskRepository.getInstance();
    ProjectTaskService projectTaskService = ProjectTaskService.getInstance();
    UserProjectTaskService userProjectTaskService = UserProjectTaskService.getInstance();

    public Task create(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.create(name);
    }

    public int createTask() {
        System.out.println("[Create task]");
        System.out.println("[Please, enter task name]");
        final String name = scanner.nextLine();
        create(name);
        System.out.println("[OK]");
        return 0;
    }

    public Task update(Long id, String name) throws TaskNotFoundException {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        return taskRepository.update(id, name);
    }

    public int updateTaskByName() throws TaskNotFoundException {
        System.out.println("[Update task]");
        System.out.println("[Please, enter task name]");
        final String name = scanner.nextLine();
        final Task task = findByName(name);
        if(task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[Please, enter task name]");
        final String new_name = scanner.nextLine();
        update(task.getId(), new_name);
        System.out.println("[OK]");
        return 0;
    }

    public int updateTaskById() throws TaskNotFoundException {
        System.out.println("[Update task]");
        System.out.println("[Please, enter task id]");
        final Long id = Long.parseLong(scanner.nextLine());
        final Task task = findById(id);
        if(task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[Please, enter task name]");
        final String name = scanner.nextLine();
        update(task.getId(), name);
        System.out.println("[OK]");
        return 0;
    }

    public int updateTaskByIndex() throws TaskNotFoundException {
        System.out.println("[Update project]");
        System.out.println("[Please, enter task index]");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Task task = findByIndex(index);
        if(task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[Please, enter task name]");
        final String name = scanner.nextLine();
        update(task.getId(), name);
        System.out.println("[OK]");
        return 0;
    }

    public Task findByIndex(int index) {
        if (index < 0 || index > taskRepository.getSize() - 1) return null;
        return taskRepository.findByIndex(index);
    }

    public Task removeByIndex(int index) {
        if (index < 0 || index > taskRepository.getSize() - 1) return null;
        return taskRepository.removeByIndex(index);
    }

    public int removeTaskByIndex() {
        System.out.println("[Please, enter task index]");
        final int index = scanner.nextInt() - 1;
        final Task task = removeByIndex(index);
        if(task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public Task findByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findByName(name);
    }

    public Task removeByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.removeByName(name);
    }

    public int removeTaskByName() {
        System.out.println("[Please, enter task name]");
        final String name = scanner.nextLine();
        final Task task = removeByName(name);
        if(task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public Task findById(Long id) throws TaskNotFoundException {
        if (id == null) return null;
        final Task task = taskRepository.findById(id);
        if (task == null) throw new TaskNotFoundException("Task not found");
        return task;
    }

    public Task removeById(Long id) throws TaskNotFoundException {
        if (id == null) return null;
        return taskRepository.removeById(id);
    }

    public int removeTaskById() throws TaskNotFoundException {
        System.out.println("[Please, enter task id]");
        final Long id = scanner.nextLong();
        final Task task;
        task = removeById(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }

    public int clearTask() {
        System.out.println("[Clear task]");
        taskRepository.clear();
        System.out.println("[OK]");
        return 0;
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public int listTask() {
        int index = 1;
        System.out.println("[List task]");
        viewTasks(findAll());
        System.out.println("[OK]");
        return 0;
    }

    public void viewTasks(final List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) return;
        int index = 1;
        for(final Task task: tasks) {
            if (task.getUserId() == null)
                System.out.println(index + "." + task.getId() + " " + task.getName());
            else
                System.out.println(index + "." + task.getId() + " " + task.getName() + " " + task.getUserId());
            index++;
        }
    }

    public void viewTask(final Task task) {
        if(task == null) return;
        System.out.println("[View task]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

    public int viewTaskByIndex() {
        System.out.println("Enter task index");
        final int index = scanner.nextInt() - 1;
        final Task task = findByIndex(index);
        viewTask(task);
        return 0;
    }

    public int viewTaskByName() {
        System.out.println("Enter task name");
        final String name = scanner.nextLine();
        final Task task = findByName(name);
        viewTask(task);
        return 0;
    }

    public int viewTaskById() throws TaskNotFoundException {
        System.out.println("Enter task id");
        final Long id = scanner.nextLong();
        final Task task = findById(id);
        viewTask(task);
        return 0;
    }

    public Task findByProjectIdAndId(Long projectId, Long id) {
        if (projectId == null || id == null) return null;
        return taskRepository.findByProjectIdAndId(projectId, id);
    }

    public List<Task> findAllByProjectId(Long projectId) {
        if (projectId == null) return null;
        return taskRepository.findAllByProjectId(projectId);
    }

    public List<Task> findAllByUserId(Long userId) {
        if (userId == null) return null;
        return taskRepository.findAllByUserId(userId);
    }

    public int addTaskToProjectByIds() throws ProjectNotFoundException, TaskNotFoundException {
        System.out.println("[Add task to project by ids]");
        System.out.println("[Please, enter project id]");
        final Long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("[Please, enter task id]");
        final Long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.addTaskToProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskFromProjectByIds() {
        System.out.println("[Remove task from project by id]");
        System.out.println("[Please, enter project id]");
        final Long projectId = Long.parseLong(scanner.nextLine());
        System.out.println("[Please, enter task id]");
        final Long taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.removeTaskFromProject(projectId, taskId);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTasksAndProject() throws ProjectNotFoundException {
        System.out.println("[Remove tasks and project by id]");
        System.out.println("[Please, enter project id]");
        final Long projectId = Long.parseLong(scanner.nextLine());
        projectTaskService.removeTasksAndProject(projectId);
        System.out.println("[OK]");
        return 0;
    }

    public int listTaskByUserId() {
        System.out.println("[List task by user id]");
        System.out.println("[Please, enter user id]");
        final Long userId = Long.parseLong(scanner.nextLine());
        final List<Task> tasks = findAllByUserId(userId);
        viewTasks(tasks);
        System.out.println("[OK]");
        return 0;
    }

    public int listTaskByProjectId() {
        System.out.println("[List task by project id]");
        System.out.println("[Please, enter project id]");
        final Long projectId = Long.parseLong(scanner.nextLine());
        final List<Task> tasks = findAllByProjectId(projectId);
        viewTasks(tasks);
        System.out.println("[OK]");
        return 0;
    }

    public int addTaskToUserByIds() throws TaskNotFoundException {
        System.out.println("[Add task to user by ids]");
        System.out.println("[Please, enter user id]");
        final Long userId = Long.parseLong(scanner.nextLine());
        System.out.println("[Please, enter task id]");
        final Long taskId = Long.parseLong(scanner.nextLine());
        userProjectTaskService.addTaskToUser(userId, taskId);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskFromUserByIds() {
        System.out.println("[Remove task from user by id]");
        System.out.println("[Please, enter user id]");
        final Long userId = Long.parseLong(scanner.nextLine());
        System.out.println("[Please, enter task id]");
        final Long taskId = Long.parseLong(scanner.nextLine());
        userProjectTaskService.removeTaskFromUser(userId, taskId);
        System.out.println("[OK]");
        return 0;
    }
}
