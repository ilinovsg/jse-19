package ru.ilinovsg.tm.observer;

import ru.ilinovsg.tm.service.SystemService;
import ru.ilinovsg.tm.service.UserService;

import static ru.ilinovsg.tm.constant.TerminalConst.*;

public class ListenerUserImpl implements Listener{
    @Override
    public int update(String command) {
        UserService userService = UserService.getInstance();

        switch (command) {
            case USER_CREATE:
                return userService.createUser();
            case USER_CLEAR:
                return userService.clearUser();
            case USER_LIST:
                return userService.listUser();
            case USER_VIEW_BY_ID:
                return userService.viewUserById();
            case USER_VIEW_BY_LOGIN:
                return userService.viewUserByLogin();
            case USER_UPDATE_BY_ID:
                return userService.updateUserById();
            case USER_UPDATE_BY_LOGIN:
                return userService.updateUserByLogin();
            case USER_REMOVE_BY_ID:
                return userService.removeUserById();
            case USER_REMOVE_BY_LOGIN:
                return userService.removeUserByLogin();
            case USER_SIGN_IN:
                return userService.signInUser();
            case USER_SIGN_OUT:
                return userService.signOutUser();
            case USER_VIEW_LOGGED:
                return userService.getCurrentUser();
            case USER_UPDATE_PASSWORD:
                return userService.updateUserPassword();
            case PROJECT_LIST_BY_CURRENT_USER:
                return userService.listProjectByCurrentUser();
            case TASK_LIST_BY_CURRENT_USER:
                return userService.listTaskByCurrentUser();
        }
        return -1;
    }
}
