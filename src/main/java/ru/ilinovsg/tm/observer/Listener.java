package ru.ilinovsg.tm.observer;

import ru.ilinovsg.tm.exception.ProjectNotFoundException;
import ru.ilinovsg.tm.exception.TaskNotFoundException;

public interface Listener {
    int update(String command) throws ProjectNotFoundException, TaskNotFoundException;
}
