package ru.ilinovsg.tm.service;

import ru.ilinovsg.tm.entity.Project;
import ru.ilinovsg.tm.entity.Task;
import ru.ilinovsg.tm.exception.ProjectNotFoundException;
import ru.ilinovsg.tm.exception.TaskNotFoundException;
import ru.ilinovsg.tm.repository.ProjectRepository;
import ru.ilinovsg.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;

public class ProjectTaskService {

    private ProjectTaskService() {
    }

    private static ProjectTaskService instance = null;

    public static ProjectTaskService getInstance(){
        synchronized (ProjectTaskService.class) {
            if (instance == null) {
                instance = new ProjectTaskService();
            }
        }
        return instance;
    }

    ProjectRepository projectRepository = ProjectRepository.getInstance();

    TaskRepository taskRepository = TaskRepository.getInstance();

    public List<Task> findAllByProjectId(final Long projectId) {
        if (projectId == null) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    public Task removeTaskFromProject (final Long projectId, final Long taskId) {
        final Task task = taskRepository.findByProjectIdAndId(projectId, taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    public Task addTaskToProject (final Long projectId, final Long taskId) throws ProjectNotFoundException, TaskNotFoundException {
        final Project project;
        project = projectRepository.findById(projectId);
        final Task task = taskRepository.findById(taskId);
        task.setProjectId(projectId);
        return task;
    }

    public void removeTasksAndProject (final Long projectId) throws ProjectNotFoundException {
        final Project project;
        project = projectRepository.findById(projectId);
        taskRepository.removeByProjectId(projectId);
        projectRepository.removeById(projectId);
        return;
    }

}
