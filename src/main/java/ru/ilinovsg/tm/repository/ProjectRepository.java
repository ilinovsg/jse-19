package ru.ilinovsg.tm.repository;

import ru.ilinovsg.tm.entity.Project;
import ru.ilinovsg.tm.exception.ProjectNotFoundException;

import java.util.*;

public class ProjectRepository {

    private ProjectRepository() {
    }

    private static ProjectRepository instance = null;

    public static ProjectRepository getInstance(){
        synchronized (ProjectRepository.class) {
            if (instance == null) {
                instance = new ProjectRepository();
            }
        }
        return instance;
    }

    private final List<Project> projects = new ArrayList<>();

    private final Map<String, Project> projectHashMap = new HashMap<>();

    public Project create(final String name) {
        final Project project = new Project(name);
        projects.add(project);
        projectHashMap.put(project.getName(), project);
        return project;
    }

    public Project create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projects.add(project);
        projectHashMap.put(project.getName(), project);
        return project;
    }

    public Project update(final Long id, final String name, final String description) throws ProjectNotFoundException {
        final Project project = findById(id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    public Project findByIndex(int index) {
        return projects.get(index);
    }

    public Project removeByIndex(final int index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        projects.remove(project);
        projectHashMap.remove(project.getName());
        return project;
    }

    public Project findByName(final String name) {
        /*for (final Project project : projects) {
            if (project.getName().equals(name)) return project;
        }
        return null;*/
        if (projectHashMap.containsKey(name)) {
            final Project project = projectHashMap.get(name);
            return project;
        }
        else return null;
    }

    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        projects.remove(project);
        projectHashMap.remove(project.getName());
        return project;
    }

    public Project findById(final Long id) throws ProjectNotFoundException {
        for (final Project project : projects) {
            if (project.getId().equals(id)) return project;
        }
        throw new ProjectNotFoundException("Project not found");
    }

    public Project removeById(final Long id) throws ProjectNotFoundException {
        final Project project = findById(id);
        projects.remove(project);
        projectHashMap.remove(project.getName());
        return project;
    }

    public List<Project> findAllByUserId(final Long userId) {
        final List<Project> result = new ArrayList<>();
        for (final Project project : findAll()) {
            final Long idUser = project.getUserId();
            if (idUser == null) continue;
            if (idUser.equals(userId)) result.add(project);
        }
        return result;
    }

    public Project removeByUserId(final Long userId) {
        for (final Project project : findAll()) {
            final Long idUser = project.getUserId();
            if (idUser == null) continue;
            if (idUser.equals(userId)) projects.remove(project);
        }
        return null;
    }

    public void clear() {
        projects.clear();
    }

    public List<Project> findAll() {
        //Collections.sort(projects); //сортировка
        return projects;
    }

    public int getSize() {
        return projects.size();
    }

    public Project findByUserIdAndId(final Long userId, final Long id) {
        for (final Project project : projects) {
            final Long idUser = project.getUserId();
            if (idUser == null) continue;
            if (!idUser.equals(userId)) continue;
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

}
