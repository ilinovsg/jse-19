package ru.ilinovsg.tm.repository;

import ru.ilinovsg.tm.entity.Task;
import ru.ilinovsg.tm.exception.TaskNotFoundException;

import java.util.*;

public class TaskRepository {

    private TaskRepository() {
    }

    private static TaskRepository instance = null;

    public static TaskRepository getInstance(){
        synchronized (TaskRepository.class) {
            if (instance == null) {
                instance = new TaskRepository();
            }
        }
        return instance;
    }

    private final List<Task> tasks = new ArrayList<>();

    private final Map<String, Task> taskHashMap = new HashMap<>();

    public Task create(final String name) {
        final Task task = new Task(name);
        tasks.add(task);
        taskHashMap.put(task.getName(), task);
        return task;
    }

    public Task update(final Long id, final String name) throws TaskNotFoundException {
        final Task task = findById(id);
        if (task == null) return null;
        task.setId(id);
        task.setName(name);
        return task;
    }

    public Task findByIndex(int index) {
        return tasks.get(index);
    }

    public Task removeByIndex(final int index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        taskHashMap.remove(task.getName());
        return task;
    }

    public Task findByName(final String name) {
        /*for (final Task task : tasks) {
            if (task.getName().equals(name)) return task;
        }
        return null;*/
        if (taskHashMap.containsKey(name)) {
            final Task task = taskHashMap.get(name);
            return task;
        }
        else return null;
    }

    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        tasks.remove(task);
        taskHashMap.remove(task.getName());
        return task;
    }

    public Task findById(final Long id) throws TaskNotFoundException {
        for (final Task task : tasks) {
            if (task.getId().equals(id)) return task;
        }
        throw new TaskNotFoundException("Task not found");
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        for (final Task task : tasks) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public Task removeById(final Long id) throws TaskNotFoundException {
        final Task task = findById(id);
        tasks.remove(task);
        taskHashMap.remove(task.getName());
        return task;
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.add(task);
        }
        return result;
    }

    public Task removeByProjectId(final Long projectId) {
        for (final Task task : findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) tasks.remove(task);
        }
        return null;
    }

    public List<Task> findAllByUserId(final Long userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long idUser = task.getUserId();
            if (idUser == null) continue;
            if (idUser.equals(userId)) result.add(task);
        }
        return result;
    }

    public Task findByUserIdAndId(final Long userId, final Long id) {
        for (final Task task : tasks) {
            final Long idUser = task.getUserId();
            if (idUser == null) continue;
            if (!idUser.equals(userId)) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public void clear() {
        tasks.clear();
    }

    public List<Task> findAll() {
        Collections.sort(tasks); //сортировка
        return tasks;
    }

    public int getSize() {
        return tasks.size();
    }

}
